import random

suits = ["♦","♣","♥","♠"]
values = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
full_deck = []
for suit in suits:
    for value in values:
        full_deck.append(suit+value)

make_deck = "Y"
while make_deck.lower() == "y":
    print()
    print("Let us construct your deck.\n")

    must_have = input("Are there any cards you must have in the deck? (Y/N)\n")
    must_have_list = []
    while must_have.lower() == "y":
        suit = input("Which suit does this card have? \n\t 1) ♦\n\t 2) ♣\n\t 3) ♥ \n\t 4) ♠\n")
        while not suit in ["1","2","3","4"]:
            print("This is not a valid suit.")
            suit = input("Which suit does this card have? \n\t 1) ♦\n\t 2) ♣\n\t 3) ♥ \n\t 4) ♠\n")
        else:
            suit = suits[int(suit)-1]
        value = input("Which value does this card have?\n")
        while not value in values + ["1", "11", "12", "13"]:
            print("This is not a valid card value. The valid values are: A (or 1), 2, 3, 4, 5, 6, 7, 8, 9, 10, J (or 11), Q (or 12), and K (or 13).")
            value = input("Which value does this card have?\n")
        else:
            if value in ["1", "11", "12", "13"]:
                value = {"1": "A", "11": "J", "12": "Q", "13": "K"}[value]
        card = suit + value
        print("You have chosen " + card + " to be a card that has to be in the deck.")
        check = input("Is this correct? (Y/N)\n")
        if not check.lower() == "y":
            print("This card has been discarded.")
            must_have = input("Would you like to enter a different card that has to be in the deck? (Y/N)\n")
        else:
            must_have_list.append(card)
            must_have = input("Would you like to enter another card that has to be in the deck? (Y/N)\n")

    print()

    must_not_have = input("Are there any cards you must not have in the deck? (Y/N)\n")
    must_not_have_list = []
    while must_not_have.lower() == "y":
        suit = input("Which suit does this card have? \n\t 1) ♦\n\t 2) ♣\n\t 3) ♥ \n\t 4) ♠\n")
        while not suit in ["1","2","3","4"]:
            print("This is not a valid suit.")
            suit = input("Which suit does this card have? \n\t 1) ♦\n\t 2) ♣\n\t 3) ♥ \n\t 4) ♠\n")
        else:
            suit = suits[int(suit)-1]
        value = input("Which value does this card have?\n")
        while not value in values + ["1", "11", "12", "13"]:
            print("This is not a valid card value. The valid values are: A (or 1), 2, 3, 4, 5, 6, 7, 8, 9, 10, J (or 11), Q (or 12), and K (or 13).")
            value = input("Which value does this card have?\n")
        else:
            if value in ["1", "11", "12", "13"]:
                value = {"1": "A", "11": "J", "12": "Q", "13": "K"}[value]
        card = suit + value
        print("You have chosen " + card + " to be a card that must not be in the deck.")
        check = input("Is this correct? (Y/N)\n")
        if not check.lower() == "y":
            print("This card has been discarded.")
            must_not_have = input("Would you like to enter a different card that must not to be in the deck? (Y/N)\n")
        else:
            must_not_have_list.append(card)
            must_not_have = input("Would you like to enter another card that must not to be in the deck? (Y/N)\n")

    print()
    print("All cards you have not entered will be optional in your deck.\n")

    optional_deck = []
    for card in full_deck:
        if not card in must_have_list:
            if not card in must_not_have_list:
                optional_deck.append(card)

    invalid = True
    while invalid:
        deck_size = input("How many cards should your deck have? (" + str(max(1,len(must_have_list))) + "-" + str(len(full_deck)) + ")\n")
        try:
            deck_size = int(deck_size)
            invalid = False
        except:
            print(deck_size + " is not a valid integer.")
        else:
            if deck_size < len(must_have_list):
                print("Your deck may not have fewer cards than the ones you require to be in it.")
                invalid = True
            if deck_size < 1:
                print("Your deck must contain at least one card.")
                invalid = True

    print()

    build_deck = "Y"
    while build_deck.lower() == "y":
        result_deck = []
        for card in must_have_list:
            result_deck.append(card)
        if deck_size > len(must_have_list):
            result_deck += random.sample(optional_deck, deck_size - len(must_have_list))
            
        shuffle_deck = "Y"
        while shuffle_deck.lower() == "y":
            random.shuffle(result_deck)

            print("Your deck of " + str(deck_size) + " has been prepared.\n")
            input("Show first card: ")
            print(result_deck[0])
            n = 1
            while n < deck_size:
                input("Show next card: ")
                print(result_deck[n])
                n += 1
            else:
                print("This was the last card.\n")
                shuffle_deck = input("Would you like to re-shuffle this deck? (Y/N)\n")
        else:
            build_deck = input("Would you like to re-build this deck? (Y/N)\n")
    else:
        make_deck = input("Would you like to make a new deck? (Y/N)\n")
else:
    print("\nHave a good day!")
